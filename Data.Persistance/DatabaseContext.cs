﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Core.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace Data.Persistance
{
    public sealed class DatabaseContext : IdentityDbContext<User, Role, Guid>
    {
        public DbSet<Auction> Auctions { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            builder.Entity<Bid>()
                .HasOne(x => x.Auction)
                .WithMany(y => y.Bids);

            builder.Entity<Bid>()
                .HasOne(x => x.Profile)
                .WithMany(y => y.Bids);

       //     builder.Entity<Profile>()
       //         .HasOne(x => x.User)
       //         .WithMany(y => y.Profiles);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    }

}
