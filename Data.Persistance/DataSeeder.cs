﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Core.Domain;
using Microsoft.AspNetCore.Identity;

namespace Data.Persistance
{
    public class DataSeeder
    {
        public void Seed(DatabaseContext context, UserManager<User> userManager)
        {
            if (!context.Users.Any())
            {
                var userType1 = new Role
                {
                    Id = Guid.Parse("56cb1501-eb4b-491f-964c-a17e528a214d"),
                    Name = "Institution"
                };
                var userType2 = new Role
                {
                    Id = Guid.Parse("600fd688-b8d1-453b-9a52-721048660b67"),
                    Name = "Bidder"
                };

                context.Roles.Add(userType1);
                context.Roles.Add(userType2);

                var user1 = new User
                {
                    Id = Guid.Parse("cc159de8-fbd7-4bd9-b33e-e7f6cc035ce2"),
                    Email = "user1@user.com",
                    UserName = "user1@user.com",
                    JoinDate = new DateTime(2012, 11, 3),
                    PhoneNumber = "702-328-5154",
                    RoleId = userType2.Id
                };
                var user2 = new User
                {
                    Id = Guid.Parse("06901a7b-6016-4f7c-88d2-65757bcd9e4b"),
                    Email = "user2@user.com",
                    UserName = "user2@user.com",
                    JoinDate = new DateTime(2014, 3, 3),
                    PhoneNumber = "864-622-5495",
                    RoleId = userType2.Id
                };
                var user3 = new User
                {
                    Id = Guid.Parse("37cfcefa-9f7d-4feb-af27-7e003a3d92c0"),
                    Email = "user3@user.com",
                    UserName = "user3@user.com",
                    JoinDate = new DateTime(2017, 12, 23),
                    PhoneNumber = "831-740-1325",
                    RoleId = userType1.Id
                };
                var user4 = new User
                {
                    Id = Guid.Parse("1374a1d5-05ca-4573-9cb8-e24080382965"),
                    Email = "user4@user.com",
                    UserName = "user4@user.com",
                    JoinDate = new DateTime(2018, 4, 14),
                    PhoneNumber = "440-245-1636",
                    RoleId = userType1.Id
                };

                userManager.CreateAsync(user1, "parola").Wait();
                userManager.CreateAsync(user2, "parola").Wait();
                userManager.CreateAsync(user3, "parola").Wait();
                userManager.CreateAsync(user4, "parola").Wait();

                var profile1 = new Profile
                {
                    Id = Guid.Parse("94da5ebc-287e-41fb-942b-81bfbe2ca1ea"),
                    BirthDay = new DateTime(1999, 12, 24),
                    FullName = "Chris A. Daniels",
                    IsDeleted = false,
                    UserId = user1.Id,
                    Photo = new Photo
                    {
                        Url = "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg"
                    }
                };
                var profile2 = new Profile
                {
                    Id = Guid.Parse("1db1a2ab-7733-43c4-a845-1fa298dcf5ee"),
                    BirthDay = new DateTime(1994, 5, 12),
                    FullName = "Anna P. Helms",
                    IsDeleted = false,
                    UserId = user2.Id,
                    Photo = new Photo
                    {
                        Url = "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg"
                    }
                };
                var profile3 = new Profile
                {
                    Id = Guid.Parse("ad68bd5e-3d92-4906-a76c-98c65b777ba7"),
                    BirthDay = new DateTime(2007, 4, 4),
                    FullName = "Mark M. Scooti",
                    IsDeleted = false,
                    UserId = user2.Id,
                    Photo = new Photo
                    {
                        Url = "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg"
                    }
                };
                var profile4 = new Profile
                {
                    Id = Guid.Parse("d3391bcb-9dd2-4936-ba35-0e0ec1e8357a"),
                    BirthDay = new DateTime(2013, 5, 21),
                    FullName = "Janna O. O'Brian",
                    IsDeleted = false,
                    UserId = user1.Id,
                    Photo = new Photo
                    {
                        Url = "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg"
                    }
                };
                var profile5 = new Profile
                {
                    Id = Guid.Parse("1e33dab1-6a61-40c0-9ea7-5e1f5d9ca3a1"),
                    BirthDay = new DateTime(1999, 11, 8),
                    FullName = "Tonny M. Core",
                    IsDeleted = false,
                    UserId = user2.Id,
                    Photo = new Photo
                    {
                        Url = "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg"
                    }
                };
                var institution1 = new Institution
                {
                    Id = Guid.Parse("18f31ccd-d051-4c71-b4d1-057df29087e8"),
                    Description = "Not available",
                    IsDeleted = false,
                    Name = "Grădinița cu program prelungit Nr. 2437",
                    City = "Iasi",
                    Country = "Romania",
                    UserId = user4.Id,
                    Photo = new Photo
                    {
                        Url = "/assets/defaults/badge1.jpg"
                    },
                    ContactPhoneNumber = "0217842187",
                    ContactEmail = "gradinita.2437@mail.com"

                };
                var institution2 = new Institution
                {
                    Id = Guid.Parse("ea657463-f505-4ef9-a384-3277cbf8bc74"),
                    Description = "Descriptionaslsdfsafaskdjsakhfa sakfhla",
                    IsDeleted = false,
                    Name = "Colegiul National",
                    City = "Iasi",
                    Country = "Romania",
                    UserId = user3.Id,
                    Photo = new Photo
                    {
                        Url = "/assets/defaults/badge1.jpg"
                    }

                };
                
                context.Profiles.Add(profile1);
                context.Profiles.Add(profile2);
                context.Profiles.Add(profile3);
                context.Profiles.Add(profile4);
                context.Profiles.Add(profile5);
                context.SaveChanges();
                context.Institutions.Add(institution1);
                context.Institutions.Add(institution2);
                context.SaveChanges();
                SeedCountries(context);
            }
            
        }

        private void SeedCountries(DatabaseContext context)
        {
            var romania = new Country
            {
                Title = "Romania"
            };
            context.Countries.Add(romania);
            context.SaveChanges();
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Bucuresti"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Cluj-Napoca"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Timisoara"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Iasi"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Constanta"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Craiova"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Brasov"
            });
            context.Cities.Add(new City
            {
                CountryId = romania.Id,
                Title = "Vaslui"
            });
            context.SaveChanges();
        }
    }
}
