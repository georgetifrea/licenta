﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Persistance.Migrations
{
    public partial class RemoveKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Auctions_Classes_ClassId",
                table: "Auctions");

            migrationBuilder.DropTable(
                name: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Auctions_ClassId",
                table: "Auctions");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Auctions");

            migrationBuilder.AddColumn<Guid>(
                name: "InstitutionId",
                table: "Auctions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxAge",
                table: "Auctions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinAge",
                table: "Auctions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Spots",
                table: "Auctions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_InstitutionId",
                table: "Auctions",
                column: "InstitutionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Auctions_Institutions_InstitutionId",
                table: "Auctions",
                column: "InstitutionId",
                principalTable: "Institutions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Auctions_Institutions_InstitutionId",
                table: "Auctions");

            migrationBuilder.DropIndex(
                name: "IX_Auctions_InstitutionId",
                table: "Auctions");

            migrationBuilder.DropColumn(
                name: "InstitutionId",
                table: "Auctions");

            migrationBuilder.DropColumn(
                name: "MaxAge",
                table: "Auctions");

            migrationBuilder.DropColumn(
                name: "MinAge",
                table: "Auctions");

            migrationBuilder.DropColumn(
                name: "Spots",
                table: "Auctions");

            migrationBuilder.AddColumn<Guid>(
                name: "ClassId",
                table: "Auctions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Classes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    InstitutionId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    MaximumAge = table.Column<int>(nullable: false),
                    MinimumAge = table.Column<int>(nullable: false),
                    Spots = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Classes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Classes_Institutions_InstitutionId",
                        column: x => x.InstitutionId,
                        principalTable: "Institutions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_ClassId",
                table: "Auctions",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_InstitutionId",
                table: "Classes",
                column: "InstitutionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Auctions_Classes_ClassId",
                table: "Auctions",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
