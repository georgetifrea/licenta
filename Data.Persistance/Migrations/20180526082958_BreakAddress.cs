﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Persistance.Migrations
{
    public partial class BreakAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InstantBuyPrice",
                table: "Auctions");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Institutions",
                newName: "Country");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "AuctionId",
                table: "Photos",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Institutions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Auctions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Institutions");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Auctions");

            migrationBuilder.RenameColumn(
                name: "Country",
                table: "Institutions",
                newName: "Address");

            migrationBuilder.AlterColumn<Guid>(
                name: "AuctionId",
                table: "Photos",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "InstantBuyPrice",
                table: "Auctions",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
