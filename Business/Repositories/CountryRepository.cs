﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;

namespace Business.Repositories
{
    public class CountryRepository : GenericRepository<Country>, ICountryRespository
    {
        public CountryRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
