﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class RoleRepository: IRoleRepository
    {
        private readonly DatabaseContext _context;
        public RoleRepository(DatabaseContext context)
        {
            _context = context;
        } 
        public Task<Role> GetRoleByName(string name)
        {
            return _context.Roles.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }
    }
}
