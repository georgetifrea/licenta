﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository(DatabaseContext context) : base(context)
        {
        }

        public Task<List<Profile>> GetProfilesForUser(Guid userId)
        {
            return _entities.Where(x => x.UserId == userId).ToListAsync();
        }

        private IQueryable<Profile> Filter(ProfileQuery filters, IQueryable<Profile> source)
        {
            if (filters.CreatedBy.HasValue)
            {
                source = source.Where(x => x.UserId == filters.CreatedBy);
            }
            if (filters.PageIndex != 0 && filters.Results != 0)
            {
                source = source.Skip((filters.PageIndex - 1) * filters.Results).Take(filters.Results);
            }
            return source;
        }

        public Task<List<Profile>> Get(ProfileQuery query)
        {
            var source = _entities.AsQueryable();
            return Filter(query, source).ToListAsync();
        }

        public async Task<bool> InActiveAuction(Guid id)
        {
            return (await _context.Bids.Where(x => x.ProfileId == id && x.Auction.EndDate > DateTime.Now)
                       .CountAsync()) > 0;
        }
    }
}
