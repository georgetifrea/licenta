﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class BidRepository : GenericRepository<Bid>, IBidRepository
    {
        public BidRepository(DatabaseContext context) : base(context)
        {

        }
        public Task<Bid> GetMaxBidderForAuction(Guid auctionId)
        {
            return _entities.Where(x => x.AuctionId == auctionId).OrderByDescending(x => x.Value).FirstOrDefaultAsync();
        }

        public Task<int> GetNumberOfBiddersForAuction(Guid auctionId)
        {
            return _entities.Where(x => x.AuctionId == auctionId).CountAsync();
        }

        public double GetLastWinningBidValue(Guid auctionId)
        {
            var auction = _context.Auctions.FirstOrDefault(x => x.Id == auctionId);
            if (auction != null)
            {
                var value =  _entities.Where(x => x.AuctionId == auctionId).OrderByDescending(x=>x.Value).ToList();
                if (value.Count < auction.Spots)
                {
                    return auction.StartingPrice;
                }
                else
                {
                    return value[auction.Spots - 1].Value;
                }
            }
            return 0;
        }

        public async Task<Bid> UpdateOrInsert(Bid bid)
        {
            Bid dbBid = null;
            var bids = await _entities.Where(x => x.AuctionId == bid.AuctionId).ToListAsync();
            var auction = _context.Auctions.FirstOrDefault(x => x.Id == bid.AuctionId);
            var maxVal = bids.Count() > 0 ? bids.Max(x => x.Value) : auction.StartingPrice;
            var profile = await _context.Profiles.FirstOrDefaultAsync(x => x.Id == bid.ProfileId);
            
            if (bid.Value > maxVal && auction?.EndDate > DateTime.Now)
            {
                dbBid = await _entities.FirstOrDefaultAsync(x =>
                    x.AuctionId == bid.AuctionId && x.ProfileId == bid.ProfileId);
                if (dbBid == null)
                {
                    dbBid = await InsertAsync(bid);
                }
                else
                {
                    dbBid.Value = bid.Value;
                    await UpdateAsync(dbBid);
                }
            }

            return dbBid;
        }

        public Task<List<Bid>> Get(BidQuery query)
        {
            var source = _entities.AsQueryable();
            return Filter(query, source).OrderByDescending(x=>x.Auction.EndDate).ToListAsync();
        }

        private IQueryable<Bid> Filter(BidQuery filters, IQueryable<Bid> source)
        {
            if (filters.CreatedBy.HasValue)
            {
                source = source.Where(x => x.Profile.UserId == filters.CreatedBy);
            }
            if (filters.PageIndex != 0 && filters.Results != 0)
            {
                source = source.Skip((filters.PageIndex - 1) * filters.Results).Take(filters.Results);
            }
            return source;
        }
    }
}
