﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class AuctionRepository : GenericRepository<Auction>, IAuctionRepository
    {
        public AuctionRepository(DatabaseContext context) : base(context)
        {
        }

        public Task<List<Auction>> Get(AuctionQuery query)
        {
            var source = _entities.AsQueryable();
            source = Sort(query, source);
            return Filter(query, source).ToListAsync();
        }

        public async Task<bool> Cancel(Guid id)
        {
            var auction = await _entities.FirstOrDefaultAsync(x => x.Id == id);
            auction.Status = "Canceled";
            _entities.Update(auction);
            return await _context.SaveChangesAsync() > 0;
        }

        private IQueryable<Auction> Filter(AuctionQuery filters, IQueryable<Auction> source)
        {
            if (filters.Active)
            {
                source = source.Where(x => x.Status.Equals("Active"));
            }
            if (filters.Completed)
            {
                source = source.Where(x => x.Status.Equals("Completed"));
            }

            if (filters.Finished)
            {
                source = source.Where(x => x.Status.Equals("Finished"));
            }
            if (!filters.City.IsNullOrEmpty())
            {
                source = source.Where(x => x.Institution.City.Equals(filters.City));
            }
            if (filters.CreatedBy.HasValue)
            {
                source = source.Where(x => x.InstitutionId == filters.CreatedBy);
            }
            if (filters.JoinedBy.HasValue)
            {
                source = source.Where(x => x.Bids.Any(y => y.Profile.UserId == filters.JoinedBy));
            }
            if (filters.StartDate > 0)
            {
                var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                var dtDateTime = dt.AddSeconds(filters.StartDate).ToLocalTime();
                source = source.Where(x => x.EndDate > dtDateTime);
            }
            if (filters.EndDate > 0)
            {
                var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                var dtDateTime = dt.AddSeconds(filters.EndDate).ToLocalTime();
                source = source.Where(x => x.EndDate < dtDateTime);
            }
            if (!filters.SearchTerm.IsNullOrEmpty())
            {
                source = source.Where(x => x.Title.Contains(filters.SearchTerm));
            }
            if (filters.PageIndex != 0 && filters.Results != 0)
            {
                source = source.Skip((filters.PageIndex - 1) * filters.Results).Take(filters.Results);
            }
            return source;
        }

        private IQueryable<Auction> Sort(AuctionQuery filters, IQueryable<Auction> source)
        {
            if (filters.Order.HasValue && filters.Criteria.HasValue)
            {
                if (filters.Order == SortOrder.Ascending)
                {
                    if (filters.Criteria == SortCriteria.Popularity)
                    {
                        source = source.OrderBy(x => x.Bids.Count);
                    }
                    if (filters.Criteria == SortCriteria.Date)
                    {
                        source = source.OrderBy(x => x.EndDate);
                    }
                }
                else
                {
                    if (filters.Criteria == SortCriteria.Popularity)
                    {
                        source = source.OrderByDescending(x => x.Bids.Count);
                    }
                    if (filters.Criteria == SortCriteria.Date)
                    {
                        source = source.OrderByDescending(x => x.EndDate);
                    }
                }
            }
            return source;
        }

        public override async Task<bool> UpdateAsync(Auction entity)
        {
            var auction = await GetByIdAsync(entity.Id);
            _context.Photos.RemoveRange(auction.Photos);
            await _context.SaveChangesAsync();

            auction.EndDate = entity.EndDate;
            auction.Description = entity.Description;
            auction.MinAge = entity.MinAge;
            auction.MaxAge = entity.MaxAge;
            auction.Spots = entity.Spots;
            auction.StartingPrice = entity.StartingPrice;
            auction.Title = entity.Title;
            auction.Photos = entity.Photos;
            _entities.Update(auction);
            
            //_context.Entry(entity).State = EntityState.Modified;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
