﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Repositories;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class CityRepository : GenericRepository<City>, ICityRepository
    {
        public CityRepository(DatabaseContext context) : base(context)
        {
        }

        public Task<List<City>> GetForCountry(Guid id)
        {
            return _entities.Where(x => x.CountryId == id).ToListAsync();
        }
    }
}
