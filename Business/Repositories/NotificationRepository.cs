﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class NotificationRepository : GenericRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(DatabaseContext context) : base(context)
        {
        }

        public Task<List<Notification>> GetAllForUser(Guid id)
        {
            
            var user = _context.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
            {
                if (user.Role.Name.Equals("Institution"))
                {
                    return _context.Notifications.Where(x => x.Auction.Institution.UserId == id &&x.Result.Equals("won")).ToListAsync();
                }
                else
                {
                    return _context.Notifications.Where(x => x.Profile.UserId == id).ToListAsync();
                }
            }
            return null;

        }
    }
}
