﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class UserRepository: IUserRepository
    {
        protected readonly DatabaseContext _context;
        protected readonly DbSet<User> _entities;

        public UserRepository(DatabaseContext context)
        {
            _context = context;
            _entities = context.Set<User>();
        }

        public async Task<User> UpdateAsync(User user)
        {
            _entities.Update(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async  Task<User> GetByIdAsync(Guid id)
        {
             return await _entities.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
