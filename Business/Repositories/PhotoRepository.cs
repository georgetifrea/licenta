﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;

namespace Business.Repositories
{
    public class PhotoRepository : GenericRepository<Photo>, IPhotoRepository
    {
        public PhotoRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
