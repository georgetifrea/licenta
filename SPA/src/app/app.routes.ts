import { Routes } from '@angular/router';
import { BaseMasterPageComponent } from './modules/shared/components/base-master-page/base-master-page.component';
import { dashboardRoutes } from './modules/dashboard/dashboard.routes';
import { profileRoutes } from './modules/profile/profile.routes';
import { accountRoutes } from './modules/account/account.routes';
import { LoginComponent } from './modules/account/components/login/login.component';
import { RegisterComponent } from './modules/account/components/register/register.component';
import { AuthGuard } from './modules/account/guards/AuthGuard.service';
import { DashboardGuard } from './modules/account/guards/DashboardGuard.service';

export const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: BaseMasterPageComponent,
    canActivate: [DashboardGuard],
    children: profileRoutes.concat(dashboardRoutes)
  }
];
