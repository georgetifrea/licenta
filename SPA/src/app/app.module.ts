import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AccountModule } from './modules/account/account.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { NotificationModule } from './modules/notifications/notification.module';
import { SharedModule } from './modules/shared/shared.module';
import { appRoutes } from './app.routes';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './modules/shared/services/auth.service';
import { ProfileModule } from './modules/profile/profile.module';
import { DashboardGuard } from './modules/account/guards/DashboardGuard.service';
import { AuthGuard } from './modules/account/guards/AuthGuard.service';
import { TokenInterceptor } from './modules/account/interceptors/token-interceptor.service';
import { JwtHelper } from 'angular2-jwt';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AccountModule,
    DashboardModule,
    NotificationModule,
    SharedModule,
    HttpClientModule,
    ProfileModule
  ],
  providers: [
    JwtHelper,
    AuthService,
    DashboardGuard,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
