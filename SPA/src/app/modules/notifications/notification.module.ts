import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyNotificationsComponent } from './components/my-notifications/my-notifications.component';
import { NotificationApiService } from './services/notification-api.service';
import { NotificationComponent } from './components/notification/notification.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [MyNotificationsComponent, NotificationComponent],
  imports: [CommonModule, RouterModule],
  providers: [NotificationApiService],
  bootstrap: []
})
export class NotificationModule {}
