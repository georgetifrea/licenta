import { Component, OnInit } from '@angular/core';
import { NotificationApiService } from '../../services/notification-api.service';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'app-my-notifications',
  templateUrl: './my-notifications.component.html',
  styleUrls: ['./my-notifications.component.scss']
})
export class MyNotificationsComponent implements OnInit {
  notifications = [];
  pageIndex;
  results;
  hasMore;
  constructor(private api: NotificationApiService, private auth: AuthService) {}

  ngOnInit() {
    this.hasMore = true;
    this.pageIndex = 1;
    this.results = 10;
    this.api.notifications
      .getAll(this.auth.currentUser().userId, this.pageIndex, this.results)
      .subscribe(response => {
        this.notifications = response;
        this.api.notifications
          .getAll(
            this.auth.currentUser().userId,
            this.pageIndex + 1,
            this.results
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onPrev() {
    this.pageIndex--;
    this.api.notifications
      .getAll(this.auth.currentUser().userId, this.pageIndex, this.results)
      .subscribe(response => {
        this.notifications = response;
        this.api.notifications
          .getAll(
            this.auth.currentUser().userId,
            this.pageIndex + 1,
            this.results
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }

  onNext() {
    this.pageIndex++;
    this.api.notifications
      .getAll(this.auth.currentUser().userId, this.pageIndex, this.results)
      .subscribe(response => {
        this.notifications = response;
        this.api.notifications
          .getAll(
            this.auth.currentUser().userId,
            this.pageIndex + 1,
            this.results
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
}
