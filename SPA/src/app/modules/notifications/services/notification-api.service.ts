import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class NotificationApiService {
  constructor(private http: HttpClient) {}
  private baseUrl = environment.apiUrl;

  public get notifications() {
    const baseUrl = this.baseUrl;
    return {
      getAll: (id: string, pageIndex: number, results: number) => {
        let url = `${baseUrl}/api/notifications/${id}?`;
        if (pageIndex) {
          url += `&pageindex=${pageIndex}`;
        }
        if (results) {
          url += `&results=${results}`;
        }
        return this.http.get<any[]>(url);
      }
    };
  }
}
