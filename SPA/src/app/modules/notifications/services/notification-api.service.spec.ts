/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotificationApiService } from './notification-api.service';

describe('Service: NotificationApi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationApiService]
    });
  });

  it('should ...', inject([NotificationApiService], (service: NotificationApiService) => {
    expect(service).toBeTruthy();
  }));
});
