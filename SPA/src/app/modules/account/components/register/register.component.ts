import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  phoneNumber: string;
  error: boolean;
  confPassword: string;
  confPasswordIsValid: boolean;
  constructor(private auth: AuthService, private router: Router) {}
  user: boolean;

  ngOnInit() {
    this.email = '';
    this.password = '';
    this.confPassword = '';
    this.confPasswordIsValid = true;
  }
  onRegisterUser() {
    this.auth
      .registerUser(this.email, this.password, this.phoneNumber)
      .subscribe(
        response => {
          this.router.navigate(['/login']);
        },
        error => {
          this.error = true;
        }
      );
  }

  onRegisterInstitution() {
    this.auth
      .registerUser(this.email, this.password, this.phoneNumber)
      .subscribe(
        response => {
          this.router.navigate(['/login']);
        },
        error => {
          this.error = true;
        }
      );
  }

  onLogin() {
    this.router.navigate(['/login']);
  }
}
