import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  error: boolean;
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit() {
    this.email = '';
    this.password = '';
  }
  onLogin() {
    this.auth.login(this.email, this.password).subscribe(
      response => {
        this.auth.saveToken(response['auth_token']);
        this.router.navigate(['/home']);
      },
      error => {
        this.error = true;
      }
    );
  }
  onRegister() {
    this.router.navigate(['/register']);
  }
}
