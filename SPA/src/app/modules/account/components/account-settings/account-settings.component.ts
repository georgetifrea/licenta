import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { AccountApiService } from '../../services/account-api.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  role: any;
  env = environment;
  email;
  phoneNumber;
  oldPassword: string;
  newPassword: string;
  confPassword: string;
  error: boolean;
  status: string;
  constructor(private auth: AuthService, private api: AccountApiService) {}

  ngOnInit() {
    this.oldPassword = '';
    this.newPassword = '';
    this.confPassword = '';
    this.api.accounts
      .getUser(this.auth.currentUser().userId)
      .subscribe(response => {
        this.email = response.email;
        this.phoneNumber = response.phoneNumber;
      });
  }
  onSave() {
    this.api.accounts
      .editUser(
        this.auth.currentUser().userId,
        this.oldPassword,
        this.newPassword
      )
      .subscribe(
        response => {
          this.error = false;
          this.status = 'Password successfully changed!';
        },
        error => {
          this.error = true;
          this.status = 'Invalid password!';
        }
      );
  }
}
