import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AccountApiService {
  constructor(private http: HttpClient) {}
  private baseUrl = environment.apiUrl;

  public get accounts() {
    const baseUrl = this.baseUrl;
    return {
      getUser: (id: string) => {
        const url = `${baseUrl}/api/auth/account/user/${id}`;
        return this.http.get<any>(url);
      },
      getInst: (id: string) => {
        const url = `${baseUrl}/api/auth/account/institution/${id}`;
        return this.http.get<any>(url);
      },
      editUser: (id: string, oldPassword: string, newPassword: string) => {
        const url = `${baseUrl}/api/auth/account/user`;
        return this.http.put<any>(url, {
          id: id,
          oldPassword: oldPassword,
          newPassword: newPassword
        });
      }
    };
  }
}
