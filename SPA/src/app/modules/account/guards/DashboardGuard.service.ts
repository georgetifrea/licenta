import { Injectable } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class DashboardGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}
  canActivate() {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
