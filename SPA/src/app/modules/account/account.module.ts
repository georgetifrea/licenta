import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { FormsModule } from '@angular/forms';
import { AccountApiService } from './services/account-api.service';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, AccountSettingsComponent],
  imports: [CommonModule, FormsModule],
  providers: [AccountApiService],
  bootstrap: []
})
export class AccountModule {}
