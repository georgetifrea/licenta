import { PhotoModel } from '../../shared/models/PhotoModel';

export class AddProfileModel {
  public userId: string;
  public fullName: string;
  public photo: PhotoModel;
  public country: string;
  public city: string;
  public birthDay: Date;
  public description: string;
}
