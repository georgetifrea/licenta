/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProfileApiService } from './profile-api.service';

describe('Service: ProfileApi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileApiService]
    });
  });

  it('should ...', inject([ProfileApiService], (service: ProfileApiService) => {
    expect(service).toBeTruthy();
  }));
});
