import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Profile } from '../models/Profile';
import { AddProfileModel } from '../models/AddProfileModel';
import { EditProfileModel } from '../models/EditProfileModel';

@Injectable()
export class ProfileApiService {
  constructor(private http: HttpClient) {}
  private baseUrl = environment.apiUrl;

  public get profiles() {
    const baseUrl = this.baseUrl;
    return {
      getMy: (id: string) => {
        const url = `${baseUrl}/api/profiles?createdby=${id}`;
        return this.http.get<Profile[]>(url);
      },
      add: (profile: AddProfileModel) => {
        const url = `${baseUrl}/api/profiles`;
        return this.http.post(url, profile);
      },
      edit: (profile: EditProfileModel) => {
        const url = `${baseUrl}/api/profiles`;
        return this.http.put(url, profile);
      },
      delete: (id: string) => {
        const url = `${baseUrl}/api/profiles/${id}`;
        return this.http.delete(url);
      },
      get: (id: string) => {
        const url = `${baseUrl}/api/profiles/${id}`;
        return this.http.get<Profile>(url);
      }
    };
  }
}
