import { Routes } from '@angular/router';
import { MyProfilesComponent } from './components/my-profiles/my-profiles.component';
import { AddProfileComponent } from './components/add-profile/add-profile.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { MyProfilesGuard } from './guards/MyProfilesGuard.service';

export const profileRoutes: Routes = [
  {
    path: 'myprofiles',
    component: MyProfilesComponent,
    canActivate: [MyProfilesGuard]
  },
  {
    path: 'myprofiles/add',
    component: AddProfileComponent,
    canActivate: [MyProfilesGuard]
  },
  {
    path: 'myprofiles/edit/:id',
    component: EditProfileComponent,
    canActivate: [MyProfilesGuard]
  }
];
