import { ProfileApiService } from './services/profile-api.service';
import { NgModule } from '@angular/core';
import { AddProfileComponent } from './components/add-profile/add-profile.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ProfileThumbnailComponent } from './components/profile-thumbnail/profile-thumbnail.component';
import { MyProfilesComponent } from './components/my-profiles/my-profiles.component';
import { CommonModule } from '@angular/common';
import { TypeaheadModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { MyProfilesGuard } from './guards/MyProfilesGuard.service';

@NgModule({
  declarations: [
    AddProfileComponent,
    EditProfileComponent,
    ProfileThumbnailComponent,
    MyProfilesComponent
  ],
  imports: [
    CommonModule,
    TypeaheadModule.forRoot(),
    FormsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [ProfileApiService, MyProfilesGuard],
  bootstrap: []
})
export class ProfileModule {}
