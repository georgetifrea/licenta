import { Component, OnInit, Input } from '@angular/core';
import { Profile } from '../../models/Profile';
import { Router } from '@angular/router';
import { ProfileApiService } from '../../services/profile-api.service';

@Component({
  selector: 'app-profile-thumbnail',
  templateUrl: './profile-thumbnail.component.html',
  styleUrls: ['./profile-thumbnail.component.scss']
})
export class ProfileThumbnailComponent implements OnInit {
  @Input() model: Profile;
  overflown = false;
  constructor(private router: Router, private api: ProfileApiService) {}

  ngOnInit() {}

  onEdit() {
    this.router.navigate([`/myprofiles/edit/${this.model.id}`]);
  }
  onDisable() {
    this.api.profiles.delete(this.model.id).subscribe(response => {
    });
  }
}
