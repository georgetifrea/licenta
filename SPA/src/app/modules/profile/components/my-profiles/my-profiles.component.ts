import { Component, OnInit } from '@angular/core';
import { Profile } from '../../models/Profile';
import { ProfileApiService } from '../../services/profile-api.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profiles',
  templateUrl: './my-profiles.component.html',
  styleUrls: ['./my-profiles.component.scss']
})
export class MyProfilesComponent implements OnInit {

  public profiles: Profile[] = [];
  constructor(private api: ProfileApiService, private auth: AuthService, private router: Router) {}

  ngOnInit() {
    this.api.profiles
      .getMy(this.auth.currentUser().userId)
      .subscribe(response => {
        this.profiles = response;
      });
  }
  onRefresh() {
    this.api.profiles
      .getMy(this.auth.currentUser().userId)
      .subscribe(response => {
        this.profiles = response;
      });
  }
  onAdd() {
    this.router.navigate(['/myprofiles/add']);
  }

}
