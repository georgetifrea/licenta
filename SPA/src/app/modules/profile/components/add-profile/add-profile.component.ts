import { Component, OnInit } from '@angular/core';
import { AddProfileModel } from '../../models/AddProfileModel';
import { AddressApiService } from '../../../shared/services/address-api.service';
import { PhotoModel } from '../../../shared/models/PhotoModel';
import { ProfileApiService } from '../../services/profile-api.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.scss']
})
export class AddProfileComponent implements OnInit {
  model: AddProfileModel;
  countries: any[] = [];
  cities: any[] = [];
  country;
  city;

  errorfn: boolean;
  errordesc: boolean;
  errorbtd: boolean;
  errorct: boolean;
  errorcy: boolean;
  constructor(
    private address: AddressApiService,
    private api: ProfileApiService,
    private router: Router,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.errorfn = false;
    this.errordesc = false;
    this.errorbtd = false;
    this.errorct = false;
    this.errorcy = false;
    this.model = new AddProfileModel();
    this.address.countries.get().subscribe(response => {
      this.countries = response;
    });
    this.model.photo = new PhotoModel();
    this.model.photo.url = '../../../../../assets/defaults/empty-profile.png';
  }
  onCreate() {
    this.model.userId = this.auth.currentUser().userId;
    if (this.validate()) {
      this.api.profiles.add(this.model).subscribe(response => {
        this.router.navigate(['/myprofiles']);
      });
    }
  }
  typeaheadCountryOnSelect($event) {
    this.address.cities.get($event.item.id).subscribe(response => {
      this.model.city = undefined;
      this.model.country = $event.item.title;
      this.cities = response;
    });
  }
  typeaheadCityOnSelect($event) {
    this.model.city = $event.item.title;
  }
  validate(): boolean {
    this.errorfn = false;
    this.errordesc = false;
    this.errorbtd = false;
    this.errorct = false;
    this.errorcy = false;
    let ok = true;
    if (!this.model.fullName || !this.model.fullName.length) {
      this.errorfn = true;
      ok = false;
    }
    if (!this.model.description || !this.model.description.length) {
      this.errordesc = true;
      ok = false;
    }

    if (!this.model.birthDay || this.model.birthDay > new Date()) {
      this.errorbtd = true;
      ok = false;
    }
    if (!this.model.country) {
      this.errorct = true;
      ok = false;
    }
    if (!this.model.city) {
      this.errorcy = true;
    }
    return ok;
  }

  onPhotoSelect(event) {
    const file = event.srcElement.files[0];
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    const model = this.model;
    reader.onload = function() {
      const x = new PhotoModel();
      x.url = 'data:image/png;base64,' + btoa(reader.result);
      model.photo = x;
    };
  }

  dateChange() {}
}
