import { SimplifiedAuction } from './SimplifiedAuction';
import { Profile } from '../../profile/models/Profile';

export class BidThumbnail {
    auction: SimplifiedAuction;
    profile: Profile;
    value: number;
}
