export class SimplifiedAuction {
  public id: string;
  public photoUrl: string;
  public title: string;
  public institutionName: string;
  public highBid: number;
}
