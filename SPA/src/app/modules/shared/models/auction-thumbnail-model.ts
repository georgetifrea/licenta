export class AuctionThumbnailModel {
  public id: string;
  public photoUrl: string;
  public title: string;
  public institutionName: string;
  public institutionId: string;
  public highestBid: number;
  public biddersCount: number;
  public spotsCount: number;
  public endDate: Date;
  public status: string;
}
