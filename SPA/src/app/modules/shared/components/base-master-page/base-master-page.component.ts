import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-base-master-page',
  templateUrl: './base-master-page.component.html',
  styleUrls: ['./base-master-page.component.scss']
})
export class BaseMasterPageComponent {
  role: string;
  env;
  constructor(private auth: AuthService, private router: Router) {
    this.env = environment;
    this.role = this.auth.getRole();
  }

  onLogout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
