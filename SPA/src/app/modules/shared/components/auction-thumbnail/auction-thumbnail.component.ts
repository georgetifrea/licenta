import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AuctionThumbnailModel } from '../../models/auction-thumbnail-model';
import { Router } from '@angular/router';
import { DashboardApiService } from '../../../dashboard/services/dashboard-api.service';
import { CountdownService } from '../../services/countdown.service';

@Component({
  selector: 'app-auction-thumbnail',
  templateUrl: './auction-thumbnail.component.html',
  styleUrls: ['./auction-thumbnail.component.scss']
})
export class AuctionThumbnailComponent implements OnInit {
  @Input() crudActions: boolean;
  @Input() model: AuctionThumbnailModel;
  @Output() refresh: EventEmitter<any> = new EventEmitter();
  enableCancel: boolean;
  enableEdit: boolean;
  cs;
  constructor(private router: Router, private api: DashboardApiService) {}

  ngOnInit() {
    if (this.crudActions) {
      const date = new Date();
      if (new Date(this.model.endDate) > date) {
        if (this.model.status === 'Active') {
          this.enableCancel = true;
          if (this.model.biddersCount === 0) {
            this.enableEdit = true;
          } else {
            this.enableEdit = false;
          }
        }
      }
    } else {
      this.enableCancel = false;
      this.enableEdit = false;
    }
    this.cs = new CountdownService();
    this.cs.start(new Date(this.model.endDate));
  }
  onCancel() {
    this.api.auctions.delete(this.model.id).subscribe(response => {
      this.refresh.emit(null);
    });
  }
  onEdit() {
    this.router.navigate([`/myauctions/edit/${this.model.id}`]);
  }
  onShow() {
    this.router.navigate([`/auctions/${this.model.id}`]);
  }
}
