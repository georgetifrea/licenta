import { Component, OnInit, Input } from '@angular/core';
import { CountdownService } from '../../services/countdown.service';

@Component({
  selector: 'app-bid-thumbnail',
  templateUrl: './bid-thumbnail.component.html',
  styleUrls: ['./bid-thumbnail.component.scss']
})
export class BidThumbnailComponent implements OnInit {
  @Input() model: any;
  countdown;
  constructor() {}

  ngOnInit() {
    this.countdown = new CountdownService();
    this.countdown.start(new Date(this.model.auction.endDate));
  }
}
