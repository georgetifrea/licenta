import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AddressApiService {
  constructor(private http: HttpClient) {}
  private baseUrl = environment.apiUrl;
  public get countries() {
    const baseUrl = this.baseUrl;
    return {
      get: () => {
        const url = `${baseUrl}/api/address/countries`;
        return this.http.get<any[]>(url);
      }
    };
  }
  public get cities() {
    const baseUrl = this.baseUrl;
    return {
      get: (id: string) => {
        const url = `${baseUrl}/api/address/cities/${id}`;
        return this.http.get<any[]>(url);
      }
    };
  }
}
