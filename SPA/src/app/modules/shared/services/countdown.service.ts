import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';

@Injectable()
export class CountdownService {
  public future: Date;
  private futureString: string;
  private counter$: Observable<number>;
  private subscription: Subscription;
  public message: string;

  constructor() {}
  dhms(t) {
    let days, hours, minutes, seconds;
    days = Math.floor(t / 86400);
    t -= days * 86400;
    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;
    if (days < 0) {
      return 'ended';
    }
    return [days + 'd', hours + 'h', minutes + 'm', seconds + 's'].join(' ');
  }
  start(date: Date) {
    this.future = date;
    this.message = this.dhms(
      Math.floor((this.future.getTime() - new Date().getTime()) / 1000)
    );
    this.counter$ = Observable.interval(1000).map(x => {
      return Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
    });

    this.subscription = this.counter$.subscribe(
      x => (this.message = this.dhms(x))
    );
  }
}
