import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {
  baseUrl: string;
  private loggedIn = false;
  constructor(private http: HttpClient, private helper: JwtHelper) {
    this.baseUrl = environment.apiUrl;
    this.loggedIn = this.isValidToken();
  }

  currentUser() {
    const data = this.helper.decodeToken(this.getToken());
    return {
      userId: data.id,
      institutionId: data.institutionId
    };
  }

  login(email: string, password: string) {
    const baseUrl = this.baseUrl + '/api/auth/login';
    return this.http.post(baseUrl, { email: email, password: password });
  }

  saveToken(token: string) {
    localStorage.setItem(environment.auth_token, token);
  }

  isAuthenticated() {
    const token = this.getToken();
    if (token && this.isValidToken()) {
      return true;
    }
    return false;
  }

  logout() {
    localStorage.removeItem(environment.auth_token);
  }

  getToken() {
    return localStorage.getItem(environment.auth_token);
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  isValidToken() {
    const token = this.getToken();
    if (token) {
      return !this.helper.isTokenExpired(token);
    }
    return false;
  }

  getRole() {
    return this.helper.decodeToken(this.getToken()).roleName;
  }

  registerUser(email: string, password: string, phoneNumber: string) {
    const baseUrl = this.baseUrl + '/api/auth/register/user';
    return this.http.post(baseUrl, {
      email: email,
      password: password,
      phoneNumber: phoneNumber
    });
  }

  registerInstitution(
    email: string,
    password: string,
    phoneNumber: string,
    name: string,
    city: string,
    country: string,
    contactPhoneNumber: string,
    contactEmail: string
  ) {
    const baseUrl = this.baseUrl + '/api/auth/register/user';
    return this.http.post(baseUrl, {
      email: email,
      password: password,
      phoneNumber: phoneNumber,
      name: name,
      city: city,
      country: country,
      contactPhoneNumber: contactPhoneNumber,
      contactEmail: contactEmail
    });
  }
}
