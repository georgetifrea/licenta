import { NgModule } from '@angular/core';
import { BaseMasterPageComponent } from './components/base-master-page/base-master-page.component';
import { CommonModule } from '@angular/common';
import { sharedRoutes } from './shared.routes';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CountdownService } from './services/countdown.service';
import { AddressApiService } from './services/address-api.service';

@NgModule({
  declarations: [BaseMasterPageComponent, NotFoundComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(sharedRoutes),
    BsDropdownModule.forRoot()
  ],
  providers: [CountdownService, AddressApiService],
  bootstrap: []
})
export class SharedModule {}
