import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { TypeaheadModule, ModalModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuctionThumbnailComponent } from '../shared/components/auction-thumbnail/auction-thumbnail.component';
import { MyAuctionsComponent } from './components/my-auctions/my-auctions.component';
import { DashboardApiService } from './services/dashboard-api.service';
import { AddAuctionComponent } from './components/add-auction/add-auction.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { EditAuctionComponent } from './components/edit-auction/edit-auction.component';
import { AuctionPageComponent } from './components/auction-page/auction-page.component';
import { RouterModule } from '@angular/router';
import { MyBidsComponent } from './components/my-bids/my-bids.component';
import { BrowsePageComponent } from './components/browse-page/browse-page.component';
import { BidModalComponent } from './components/bid-modal/bid-modal.component';
import { BidProfileComponent } from './components/bid-profile/bid-profile.component';
import { MyAuctionsGuard } from './guards/MyAuctionsGuard.service';
import { MyBidsGuard } from './guards/MyBidsGuard.service';
import { BidThumbnailComponent } from '../shared/components/bid-thumbnail/bid-thumbnail.component';

@NgModule({
  declarations: [
    HomeComponent,
    AuctionThumbnailComponent,
    MyAuctionsComponent,
    AddAuctionComponent,
    EditAuctionComponent,
    AuctionPageComponent,
    MyBidsComponent,
    BrowsePageComponent,
    BidModalComponent,
    BidProfileComponent,
    BidThumbnailComponent
  ],
  imports: [
    CommonModule,
    TypeaheadModule,
    ReactiveFormsModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    RouterModule,
    ModalModule.forRoot()
  ],
  providers: [DashboardApiService, MyAuctionsGuard, MyBidsGuard],
  bootstrap: [],
  entryComponents: [BidModalComponent]
})
export class DashboardModule {}
