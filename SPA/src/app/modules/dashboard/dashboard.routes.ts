import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MyAuctionsComponent } from './components/my-auctions/my-auctions.component';
import { AddAuctionComponent } from './components/add-auction/add-auction.component';
import { EditAuctionComponent } from './components/edit-auction/edit-auction.component';
import { AuctionPageComponent } from './components/auction-page/auction-page.component';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';
import { MyBidsComponent } from './components/my-bids/my-bids.component';
import { BrowsePageComponent } from './components/browse-page/browse-page.component';
import { AccountSettingsComponent } from '../account/components/account-settings/account-settings.component';
import { MyAuctionsGuard } from './guards/MyAuctionsGuard.service';
import { MyBidsGuard } from './guards/MyBidsGuard.service';
import { MyNotificationsComponent } from '../notifications/components/my-notifications/my-notifications.component';

export const dashboardRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'account',
    component: AccountSettingsComponent
  },
  {
    path: 'auctions',
    component: BrowsePageComponent
  },
  {
    path: 'mybids',
    component: MyBidsComponent,
    canActivate: [MyBidsGuard]
  },

  {
    path: 'myauctions',
    component: MyAuctionsComponent,
    canActivate: [MyAuctionsGuard]
  },
  {
    path: 'myauctions/add',
    component: AddAuctionComponent,
    canActivate: [MyAuctionsGuard]
  },
  {
    path: 'myauctions/edit/:id',
    component: EditAuctionComponent,
    canActivate: [MyAuctionsGuard]
  },
  {
    path: 'auctions/:id',
    component: AuctionPageComponent
  },
  {
    path: 'mynotifications',
    component: MyNotificationsComponent,
  },
  {
    path: '**',
    component: NotFoundComponent
  }

];
