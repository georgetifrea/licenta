import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { environment } from '../../../../environments/environment';

@Injectable()
export class MyAuctionsGuard implements CanActivate {

    constructor(private auth: AuthService, private router: Router) {}
    canActivate() {
      if (this.auth.getRole() !== environment.institution) {
        this.router.navigate(['/home']);
        return false;
      }
      return true;
    }

}
