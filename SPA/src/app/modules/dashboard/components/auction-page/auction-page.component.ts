import { Component, OnInit } from '@angular/core';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Auction } from '../../models/Auction';
import { AuthService } from '../../../shared/services/auth.service';
import { Institution } from '../../models/Institution';
import { PhotoModel } from '../../../shared/models/PhotoModel';
import { CountdownService } from '../../../shared/services/countdown.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { BidModalComponent } from '../bid-modal/bid-modal.component';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-auction-page',
  templateUrl: './auction-page.component.html',
  styleUrls: ['./auction-page.component.scss']
})
export class AuctionPageComponent implements OnInit {
  model: Auction;
  spots = [];
  cs;
  bsModalRef: BsModalRef;
  env;
  constructor(
    private api: DashboardApiService,
    private router: Router,
    public auth: AuthService,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.env = environment;
    this.model = new Auction();
    this.model.institution = new Institution();
    this.model.photos = [];
    this.model.institution.photo = new PhotoModel();
    this.route.params.subscribe(params => {
      this.api.auctions.get(params['id']).subscribe(response => {
        this.model = response;
        this.spots = Array(this.model.spots).fill(4);
        this.cs = new CountdownService();
        this.cs.start(new Date(this.model.endDate));
      });
    });
  }
  onJoin() {
    const initialState = { auctionId: this.model.id };
    this.bsModalRef = this.modalService.show(BidModalComponent, {
      initialState
    });
    this.bsModalRef.content.closed.subscribe(value => {
      this.model = new Auction();
      this.model.institution = new Institution();
      this.model.photos = [];
      this.model.institution.photo = new PhotoModel();
      this.route.params.subscribe(params => {
        this.api.auctions.get(params['id']).subscribe(response => {
          this.model = response;
          this.spots = Array(this.model.spots).fill(4);
          this.cs = new CountdownService();
          this.cs.start(new Date(this.model.endDate));
        });
      });
    });
  }
}
