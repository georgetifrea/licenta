import { Component, OnInit } from '@angular/core';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-bids',
  templateUrl: './my-bids.component.html',
  styleUrls: ['./my-bids.component.scss']
})
export class MyBidsComponent implements OnInit {
  public bids = [];
  page: number;
  results: number;
  constructor(
    private api: DashboardApiService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.page = 1;
    this.results = 10;
    this.api.bids
      .get(this.auth.currentUser().userId, this.page, this.results)
      .subscribe(response => {
        this.bids = response;
      });
  }
  onRefresh() {}
  onPrev() {
    this.page--;
    this.api.bids
      .get(this.auth.currentUser().userId, this.page, this.results)
      .subscribe(response => {
        this.bids = response;
      });
  }
  onNext() {
    this.page++;
    this.api.bids
      .get(this.auth.currentUser().userId, this.page, this.results)
      .subscribe(response => {
        this.bids = response;
      });
  }
}
