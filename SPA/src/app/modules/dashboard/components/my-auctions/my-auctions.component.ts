import { Component, OnInit } from '@angular/core';
import { AuctionThumbnailModel } from '../../../shared/models/auction-thumbnail-model';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-auctions',
  templateUrl: './my-auctions.component.html',
  styleUrls: ['./my-auctions.component.scss']
})
export class MyAuctionsComponent implements OnInit {
  public auctions: AuctionThumbnailModel[] = [];
  page: number;
  results: number;
  hasMore;
  constructor(
    private api: DashboardApiService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.hasMore = true;
    this.page = 1;
    this.results = 10;
    this.api.auctions
      .getAllFiltered(
        null,
        null,
        null,
        null,
        null,
        this.auth.currentUser().institutionId,
        this.page,
        this.results,
        'date',
        'descending',
        null,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            null,
            null,
            null,
            null,
            null,
            this.auth.currentUser().institutionId,
            this.page,
            this.results,
            'date',
            'descending',
            null,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onRefresh() {
    this.api.auctions
      .getAllFiltered(
        null,
        null,
        null,
        null,
        null,
        this.auth.currentUser().institutionId,
        this.page,
        this.results,
        'date',
        'descending',
        null,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            null,
            null,
            null,
            null,
            null,
            this.auth.currentUser().institutionId,
            this.page,
            this.results,
            'date',
            'descending',
            null,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onAdd() {
    this.router.navigate(['/myauctions/add']);
  }
  onPrev() {
    this.page--;
    this.api.auctions
      .getAllFiltered(
        null,
        null,
        null,
        null,
        null,
        this.auth.currentUser().institutionId,
        this.page,
        this.results,
        'date',
        'v',
        null,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            null,
            null,
            null,
            null,
            null,
            this.auth.currentUser().institutionId,
            this.page,
            this.results,
            'date',
            'descending',
            null,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onNext() {
    this.page++;
    this.api.auctions
      .getAllFiltered(
        null,
        null,
        null,
        null,
        null,
        this.auth.currentUser().institutionId,
        this.page,
        this.results,
        'date',
        'descending',
        null,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            null,
            null,
            null,
            null,
            null,
            this.auth.currentUser().institutionId,
            this.page,
            this.results,
            'date',
            'descending',
            null,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
}
