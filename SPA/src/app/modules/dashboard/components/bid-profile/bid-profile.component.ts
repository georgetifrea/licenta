import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { BidModel } from '../../models/BidModel';

@Component({
  selector: 'app-bid-profile',
  templateUrl: './bid-profile.component.html',
  styleUrls: ['./bid-profile.component.scss']
})
export class BidProfileComponent implements OnInit {
  @Input() profile;
  @Input() auctionId;
  @Output() close = new EventEmitter<any>();
  value: number;
  error: boolean;
  disabled: boolean;
  constructor(private api: DashboardApiService) {}

  ngOnInit() {
    this.disabled = false;
    this.api.auctions.get(this.auctionId).subscribe(response => {
      const ageDifMs = Date.now() - new Date(this.profile.birthDay).getTime();
      const ageDate = new Date(ageDifMs);
      const age = Math.abs(ageDate.getUTCFullYear() - 1970);
      if (response.maxAge && response.minAge) {
        if (age < response.minAge || age > response.maxAge) {
          this.disabled = true;
        }
      }
    });
    this.error = false;
  }
  onJoin() {
    const bid = new BidModel();
    bid.auctionId = this.auctionId;
    bid.profileid = this.profile.id;
    bid.value = this.value;
    this.api.bids.add(bid).subscribe(
      response => {
        this.error = false;
        this.close.emit(null);
      },
      error => {
        this.error = true;
      }
    );
  }
}
