import { Component, OnInit } from '@angular/core';
import { AddAuctionModel } from '../../models/AddAuctionModel';
import { PhotoModel } from '../../../shared/models/PhotoModel';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-add-auction',
  templateUrl: './add-auction.component.html',
  styleUrls: ['./add-auction.component.scss']
})
export class AddAuctionComponent implements OnInit {
  auction: AddAuctionModel;
  titleErr;
  endDErr;
  spotsErr;
  priceErr;
  phohoErr;
  descErr;
  minErr;
  maxErr;

  constructor(
    private api: DashboardApiService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.titleErr = false;
    this.endDErr = false;
    this.spotsErr = false;
    this.priceErr = false;
    this.phohoErr = false;
    this.descErr = false;
    this.minErr = false;
    this.maxErr = false;

    this.auction = new AddAuctionModel();
    const date = new Date();
    date.setDate(date.getDate() + 1);
    this.auction.endDate = date;
    this.auction.photos = [];
    this.auction.institutionId = this.auth.currentUser().institutionId;
  }
  onSelect(event) {
    const file = event.srcElement.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsBinaryString(file);
      const photos = this.auction.photos;
      reader.onload = function() {
        const x = new PhotoModel();
        x.url = 'data:image/png;base64,' + btoa(reader.result);
        photos.push(x);
      };
    }
  }
  onCreate() {
    if (this.validate()) {
      this.api.auctions.add(this.auction).subscribe(response => {
        this.router.navigate([`/myauctions`]);
      });
    }
  }
  validate(): boolean {
    this.titleErr = false;
    this.endDErr = false;
    this.spotsErr = false;
    this.priceErr = false;
    this.phohoErr = false;
    this.descErr = false;
    this.minErr = false;
    this.maxErr = false;

    let ok = true;
    if (!this.auction.title || !this.auction.title.length) {
      this.titleErr = true;
      ok = false;
    }
    if (!this.auction.description || !this.auction.description.length) {
      this.descErr = true;
      ok = false;
    }
    if (this.auction.endDate < new Date()) {
      this.endDErr = true;
      ok = false;
    }
    if (!this.auction.spots || this.auction.spots < 1) {
      this.spotsErr = true;
      ok = false;
    }
    if (!this.auction.startingPrice || this.auction.startingPrice < 1) {
      this.priceErr = true;
      ok = false;
    }
    if (this.auction.hasRequirements) {
      if (
        !this.auction.minAge ||
        this.auction.minAge < 1 ||
        this.auction.minAge > this.auction.maxAge
      ) {
        this.minErr = true;
        ok = false;
      }
      if (
        !this.auction.maxAge ||
        this.auction.maxAge < 1 ||
        this.auction.minAge > this.auction.maxAge
      ) {
        this.maxErr = true;
        ok = false;
      }
    }
    if (!this.auction.photos.length) {
      this.phohoErr = true;
      ok = false;
    }
    return ok;
  }
  removeSlide() {
    const index = $('#carouselExampleControls .active').index(
      '#carouselExampleControls .carousel-item'
    );
    this.auction.photos.splice(index, 1);
    if (this.auction.photos.length) {
      $('#carouselExampleControls .carousel-item')
        .first()
        .addClass('active');
    }
  }
}
