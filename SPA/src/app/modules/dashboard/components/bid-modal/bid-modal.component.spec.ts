/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BidModalComponent } from './bid-modal.component';

describe('BidModalComponent', () => {
  let component: BidModalComponent;
  let fixture: ComponentFixture<BidModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
