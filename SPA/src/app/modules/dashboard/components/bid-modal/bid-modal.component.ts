import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { AuthService } from '../../../shared/services/auth.service';
import { ProfileApiService } from '../../../profile/services/profile-api.service';

@Component({
  selector: 'app-bid-modal',
  templateUrl: './bid-modal.component.html',
  styleUrls: ['./bid-modal.component.scss']
})
export class BidModalComponent implements OnInit {
  auctionId: any;
  userId: any;
  profiles: any;
  @Output() closed: EventEmitter<any> = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    private auth: AuthService,
    private papi: ProfileApiService
  ) {}

  ngOnInit() {
    this.userId = this.auth.currentUser().userId;
    this.papi.profiles.getMy(this.userId).subscribe(response => {
      this.profiles = response;
    });
  }
  close() {
    this.bsModalRef.hide();
    this.closed.emit(null);
  }
}
