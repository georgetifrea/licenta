import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { AuctionThumbnailModel } from '../../../shared/models/auction-thumbnail-model';
import { DashboardApiService } from '../../services/dashboard-api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  auctions: AuctionThumbnailModel[] = [];
  constructor(private api: DashboardApiService) {}

  ngOnInit() {
    this.api.auctions
      .getAllFiltered(
        null,
        null,
        null,
        null,
        null,
        null,
        1,
        2,
        'popularity',
        'descending',
        true,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
      });
  }
}
