import { Component, OnInit } from '@angular/core';
import { PhotoModel } from '../../../shared/models/PhotoModel';
import { EditAuctionModel } from '../../models/EditAuctionModel';
import { DashboardApiService } from '../../services/dashboard-api.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-edit-auction',
  templateUrl: './edit-auction.component.html',
  styleUrls: ['./edit-auction.component.scss']
})
export class EditAuctionComponent implements OnInit {
  auction: EditAuctionModel;
  titleErr;
  endDErr;
  spotsErr;
  priceErr;
  phohoErr;
  descErr;
  minErr;
  maxErr;

  constructor(
    private api: DashboardApiService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.titleErr = false;
    this.endDErr = false;
    this.spotsErr = false;
    this.priceErr = false;
    this.phohoErr = false;
    this.descErr = false;
    this.minErr = false;
    this.maxErr = false;

    this.auction = new EditAuctionModel();
    this.auction.photos = [];
    this.route.params.subscribe(params => {
      this.api.auctions.get(params['id']).subscribe(response => {
        this.auction = response;
        this.auction.endDate = new Date(response.endDate);
        this.auction.hasRequirements = !!this.auction.minAge;
        if (
          this.auction.institutionId !== this.auth.currentUser().institutionId
        ) {
          this.router.navigate(['/home']);
        }
      });
    });
  }
  onSelect(event) {
    const file = event.srcElement.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsBinaryString(file);
      const photos = this.auction.photos;
      reader.onload = function() {
        const x = new PhotoModel();
        x.url = 'data:image/png;base64,' + btoa(reader.result);
        photos.push(x);
      };
    }
  }
  onSave() {
    if (this.validate()) {
      this.api.auctions.edit(this.auction).subscribe(response => {
        this.router.navigate(['/myauctions']);
      });
    }
  }
  validate(): boolean {
    this.titleErr = false;
    this.endDErr = false;
    this.spotsErr = false;
    this.priceErr = false;
    this.phohoErr = false;
    this.descErr = false;
    this.minErr = false;
    this.maxErr = false;

    let ok = true;
    if (!this.auction.title || !this.auction.title.length) {
      this.titleErr = true;
      ok = false;
    }
    if (!this.auction.description || !this.auction.description.length) {
      this.descErr = true;
      ok = false;
    }
    if (this.auction.endDate < new Date()) {
      this.endDErr = true;
      ok = false;
    }
    if (!this.auction.spots || this.auction.spots < 1) {
      this.spotsErr = true;
      ok = false;
    }
    if (!this.auction.startingPrice || this.auction.startingPrice < 1) {
      this.priceErr = true;
      ok = false;
    }
    if (this.auction.hasRequirements) {
      if (
        !this.auction.minAge ||
        this.auction.minAge < 1 ||
        this.auction.minAge > this.auction.maxAge
      ) {
        this.minErr = true;
        ok = false;
      }
      if (
        !this.auction.maxAge ||
        this.auction.maxAge < 1 ||
        this.auction.minAge > this.auction.maxAge
      ) {
        this.maxErr = true;
        ok = false;
      }
    }
    if (!this.auction.photos.length) {
      this.phohoErr = true;
      ok = false;
    }
    return ok;
  }
  removeSlide() {
    const index = $('#carouselExampleControls .active').index(
      '#carouselExampleControls .carousel-item'
    );
    this.auction.photos.splice(index, 1);
    if (this.auction.photos.length) {
      $('#carouselExampleControls .carousel-item')
        .first()
        .addClass('active');
    }
  }
}
