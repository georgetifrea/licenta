import { Component, OnInit } from '@angular/core';
import { AddressApiService } from '../../../shared/services/address-api.service';
import { FormControl, FormGroup } from '@angular/forms';
import { DashboardApiService } from '../../services/dashboard-api.service';

@Component({
  selector: 'app-browse-page',
  templateUrl: './browse-page.component.html',
  styleUrls: ['./browse-page.component.scss']
})
export class BrowsePageComponent implements OnInit {
  countries: any[] = [];
  cities: any[] = [];
  country;
  city;
  startDate;
  endDate;
  searchInput;
  auctions = [];
  page: number;
  results: number;

  hasMore;

  countryControl = new FormControl();
  countryForm = new FormGroup({
    country: this.countryControl
  });

  cityControl = new FormControl();
  cityForm = new FormGroup({
    city: this.cityControl
  });
  constructor(
    private address: AddressApiService,
    private api: DashboardApiService
  ) {}

  ngOnInit() {
    this.hasMore = true;
    this.page = 1;
    this.results = 10;
    this.address.countries.get().subscribe(response => {
      this.countries = response;
    });
    this.api.auctions
      .getAllFiltered(
        this.country,
        this.city,
        this.startDate,
        this.endDate,
        this.searchInput,
        null,
        this.page,
        this.results,
        'date',
        'ascending',
        true,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            this.country,
            this.city,
            this.startDate,
            this.endDate,
            this.searchInput,
            null,
            this.page,
            this.results,
            'date',
            'ascending',
            true,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  typeaheadCountryOnSelect($event, countryInput, cityInput) {
    this.address.cities.get($event.item.id).subscribe(response => {
      this.city = undefined;
      this.country = $event.item.title;
      this.cities = response;
      cityInput.value = '';
    });
  }
  typeaheadCityOnSelect($event, cityInput) {
    this.city = $event.item.title;
    // input.value = '';
  }
  onFilter() {
    this.page = 1;
    this.api.auctions
      .getAllFiltered(
        this.country,
        this.city,
        this.startDate,
        this.endDate,
        this.searchInput,
        null,
        this.page,
        this.results,
        'date',
        'ascending',
        true,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            this.country,
            this.city,
            this.startDate,
            this.endDate,
            this.searchInput,
            null,
            this.page,
            this.results,
            'date',
            'ascending',
            true,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onPrev() {
    this.page--;
    this.api.auctions
      .getAllFiltered(
        this.country,
        this.city,
        this.startDate,
        this.endDate,
        this.searchInput,
        null,
        this.page,
        this.results,
        'date',
        'ascending',
        true,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            this.country,
            this.city,
            this.startDate,
            this.endDate,
            this.searchInput,
            null,
            this.page,
            this.results,
            'date',
            'ascending',
            true,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
  onNext() {
    this.page++;
    this.api.auctions
      .getAllFiltered(
        this.country,
        this.city,
        this.startDate,
        this.endDate,
        this.searchInput,
        null,
        this.page,
        this.results,
        'date',
        'ascending',
        true,
        null,
        null
      )
      .subscribe(response => {
        this.auctions = response;
        this.api.auctions
          .getAllFiltered(
            this.country,
            this.city,
            this.startDate,
            this.endDate,
            this.searchInput,
            null,
            this.page,
            this.results,
            'date',
            'ascending',
            true,
            null,
            null
          )
          .subscribe(rsp => {
            this.hasMore = rsp.length > 0;
          });
      });
  }
}
