import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { AuctionThumbnailModel } from '../../shared/models/auction-thumbnail-model';
import { AddAuctionModel } from '../models/AddAuctionModel';
import { EditAuctionModel } from '../models/EditAuctionModel';
import { Auction } from '../models/Auction';
import { BidModel } from '../models/BidModel';
import { BidThumbnail } from '../../shared/models/BidThumbnail';

@Injectable()
export class DashboardApiService {
  constructor(private http: HttpClient) {}
  private baseUrl = environment.apiUrl;

  public get bids() {
    const baseUrl = this.baseUrl;
    return {
      add: (bid: BidModel) => {
        const url = `${baseUrl}/api/bids`;
        return this.http.post(url, bid);
      },
      get: (createdby?, page?, count?) => {
        let url = `${baseUrl}/api/bids?`;
        if (createdby) {
          url += `&createdby=${createdby}`;
        }
        if (page) {
          url += `&pageindex=${page}`;
        }
        if (count) {
          url += `&results=${count}`;
        }
        return this.http.get<BidThumbnail[]>(url);
      }
    };
  }
  public get auctions() {
    const baseUrl = this.baseUrl;
    return {
      add: (auction: AddAuctionModel) => {
        const url = `${baseUrl}/api/auctions`;
        return this.http.post(url, auction);
      },
      get: (id: string) => {
        const url = `${baseUrl}/api/auctions/${id}`;
        return this.http.get<Auction>(url);
      },
      edit: (auction: EditAuctionModel) => {
        const url = `${baseUrl}/api/auctions`;
        return this.http.put(url, auction);
      },
      delete: (id: string) => {
        const url = `${baseUrl}/api/auctions/${id}`;
        return this.http.delete(url);
      },
      getAllFiltered: (
        country?,
        city?,
        startDate?,
        endDate?,
        searchTerm?,
        me?,
        page?,
        count?,
        criteria?,
        order?,
        active?,
        completed?,
        finished?
      ) => {
        let url = `${baseUrl}/api/auctions?`;
        if (country) {
          url += `&country=${country}`;
        }
        if (city) {
          url += `&city=${city}`;
        }
        if (startDate) {
          url += `&startdate=${new Date(startDate).getTime() / 1000}`;
        }
        if (endDate) {
          url += `&enddate=${new Date(endDate).getTime() / 1000}`;
        }
        if (me) {
          url += `&createdby=${me}`;
        }
        if (page) {
          url += `&pageindex=${page}`;
        }
        if (count) {
          url += `&results=${count}`;
        }
        if (searchTerm) {
          url += `&searchterm=${searchTerm}`;
        }
        if (criteria) {
          url += `&criteria=${criteria}`;
        }
        if (order) {
          url += `&order=${order}`;
        }
        if (active) {
          url += `&active=${active}`;
        }
        if (completed) {
          url += `&completed=${completed}`;
        }
        if (finished) {
          url += `&finished=${finished}`;
        }

        return this.http.get<AuctionThumbnailModel[]>(url);
      }
    };
  }
}
