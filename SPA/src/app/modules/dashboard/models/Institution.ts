import { PhotoModel } from '../../shared/models/PhotoModel';

export class Institution {
  public name: string;
  public description: string;
  public photo: PhotoModel;
  public country: string;
  public city: string;
  public email: string;
  public phoneNumber: string;
}
