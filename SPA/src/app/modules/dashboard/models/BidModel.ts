export class BidModel {
  public auctionId: string;
  public profileid: string;
  public value: number;
}
