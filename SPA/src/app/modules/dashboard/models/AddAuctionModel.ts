import { PhotoModel } from '../../shared/models/PhotoModel';

export class AddAuctionModel {
  public institutionId: string;
  public title: string;
  public startingPrice: number;
  public endDate: Date;
  public spots: number;
  public description: string;
  public hasRequirements: boolean;
  public minAge: number;
  public maxAge: number;
  public photos: PhotoModel[];
}
