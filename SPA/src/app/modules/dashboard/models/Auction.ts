import { PhotoModel } from '../../shared/models/PhotoModel';
import { Institution } from './Institution';

export class Auction {
  public id: string;
  public title: string;
  public currentMaxBid: number;
  public startDate: Date;
  public endDate: Date;
  public spots: number;
  public description: string;
  public photos: PhotoModel[];
  public isMaxBidder: boolean;
  public numberOfBidders: number;
  public startingPrice: number;
  public minAge: number;
  public maxAge: number;
  public hasRequirements: boolean;
  public institution: Institution;
  public institutionId: string;
}
