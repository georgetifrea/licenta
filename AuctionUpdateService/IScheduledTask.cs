﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Data.Persistance;

namespace AuctionUpdateService
{
    public interface IScheduledTask
    {
        void LoopUpdater(DatabaseContext context);
    }
}
