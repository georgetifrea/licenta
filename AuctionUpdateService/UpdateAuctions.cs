﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Requests;
using Data.Persistance;

namespace AuctionUpdateService
{
    public class UpdateAuctions : IScheduledTask
    {
        public void LoopUpdater(DatabaseContext context)
        {
            while (true)
            {
                var items = context.Auctions.Where(x => x.Status.Equals("Active") && x.EndDate < DateTime.Now).ToList();
                foreach (var item in items)
                {
                    item.Status = "Completed";

                    var value = item.Bids.OrderByDescending(x => x.Value).ToList();
                    double acceptedValue = 0;
                    if (value.Count < item.Spots)
                    {
                        acceptedValue = item.StartingPrice;
                    }
                    else
                    {
                        acceptedValue = value[item.Spots - 1].Value;
                    }

                    foreach (var bid in item.Bids)
                    {
                        if (bid.Value >= acceptedValue)
                        {
                            context.Notifications.Add(new Notification
                            {
                                AuctionId = item.Id,
                                ProfileId = bid.ProfileId,
                                Message = String.Format("{0} is the winner of the auction {1}, by betting {2}$",
                                bid.Profile.FullName,
                                item.Title,
                                bid.Value
                                ),
                                SentAt = DateTime.Now,
                                Status = "Not seen",
                                Result = "won"
                            });
                        }
                        else
                        {
                            context.Notifications.Add(new Notification
                            {
                                AuctionId = item.Id,
                                ProfileId = bid.ProfileId,
                                Message = String.Format("{0} lost the auction {1} because he/she only bet {2}$",
                                    bid.Profile.FullName,
                                    item.Title,
                                    bid.Value
                                ),
                                SentAt = DateTime.Now,
                                Status = "Not seen",
                                Result = "lost"
                            });
                        }
                    }
                }
                context.SaveChanges();
                Thread.Sleep(2000);
            }
        }


    }
}
