﻿using System;
using AuctionUpdateService;
using Data.Persistance;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DatabaseContext>();
            builder.UseSqlServer(GetConnectionString());
            var update = new UpdateAuctions();
            using (var context = new DatabaseContext(builder.Options))
            {
                update.LoopUpdater(context);
            }
        }

        public static string GetConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            return configuration.GetConnectionString("DefaultConnection");
        }
    }
}
