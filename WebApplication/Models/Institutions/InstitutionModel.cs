﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Institutions
{
    public class InstitutionModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public PhotoModel Photo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}
