﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Institutions
{
    public static class Mapper
    {
        public static InstitutionModel Map(this Institution institution)
        {
            return new InstitutionModel
            {
                City = institution.City,
                Country = institution.Country,
                Description = institution.Description,
                Name = institution.Name,
                PhoneNumber = institution.ContactPhoneNumber,
                Email = institution.ContactEmail,
                Photo = institution.Photo.Map()
            };
        }
    }
}
