﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Registrations
{
    public static class Mapper
    {
        public static User Map(this BidderRegistrationModel userModel)
        {
            return new User
            {
                Email = userModel.Email,
                UserName = userModel.Email,
                JoinDate = DateTime.Now,
                PhoneNumber = userModel.PhoneNumber
            };
        }

        public static User Map(this InstitutionRegistrationModel userModel)
        {
            return new User
            {
                Email = userModel.Email,
                UserName = userModel.Email,
                JoinDate = DateTime.Now,
                PhoneNumber = userModel.PhoneNumber,
                Institutions = new List<Institution>
                {
                    new Institution
                    {
                        City = userModel.City,
                        Country = userModel.Country,
                        ContactPhoneNumber = userModel.ContactPhoneNumber,
                        ContactEmail = userModel.ContactEmail,
                        Name = userModel.Name,
                        Photo = new Photo
                        {
                            Url = "/assets/defaults/badge1.jpg"
                        },
                        Description = ""
                    }
                }
            };
        }

        public static EditUserModel Map2(this User user)
        {
            return new EditUserModel
            {
                Email = user.Email,
                Id = user.Id,
                PhoneNumber = user.PhoneNumber
            };
        }
    }
}
