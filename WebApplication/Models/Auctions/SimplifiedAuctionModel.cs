﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models.Auctions
{
    public class SimplifiedAuctionModel
    {
        public Guid Id { get; set; }
        public string PhotoUrl { get; set; }
        public string Title { get; set; }
        public string InstitutionName { get; set; }
        public double HighBid { get; set; }
        public DateTime EndDate { get; set; }
    }
}
