﻿using System;
using System.Collections.Generic;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Auctions
{
    public class AddAuctionModel
    {
        public Guid InstitutionId { get; set; }
        public string Title { get; set; }
        public double StartingPrice { get; set; }
        public DateTime EndDate { get; set; }
        public int Spots { get; set; }
        public string Description { get; set; }
        public bool HasRequirements { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public virtual List<PhotoModel> Photos { get; set; }
    }
}
