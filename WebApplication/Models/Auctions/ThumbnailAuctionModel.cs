﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models.Auctions
{
    public class ThumbnailAuctionModel
    {
        public Guid Id { get; set; }
        public string PhotoUrl { get; set; }
        public string Title { get; set; }
        public string InstitutionName { get; set; }
        public Guid InstitutionId { get; set; }
        public double? HighestBid { get; set; }
        public int BiddersCount { get; set; }
        public int SpotsCount { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
    }
}
