﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Institutions;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Auctions
{
    public static class Mapper
    {
        public static Auction Map(this AddAuctionModel model)
        {
            return new Auction
            {
                InstitutionId = model.InstitutionId,
                Title = model.Title,
                StartingPrice = model.StartingPrice,
                StartDate = DateTime.Now,
                EndDate = model.EndDate,
                Status = "Active",
                Spots = model.Spots,
                Description = model.Description,
                Photos = model.Photos.Select(x => x.Map()).ToList(),
                MinAge = model.HasRequirements ? model.MinAge : null,
                MaxAge = model.HasRequirements ? model.MaxAge : null
            };
        }

        public static GetAuctionModel Map(this Auction auction)
        {
            return new GetAuctionModel
            {
                Id = auction.Id,
                Institution = auction.Institution.Map(),
                InstitutionId = auction.InstitutionId,
                Title = auction.Title,
                StartDate = auction.StartDate,
                EndDate = auction.EndDate,
                Status = auction.Status,
                Spots = auction.Spots,
                Description = auction.Description,
                Photos = auction.Photos.Select(x => x.Map()).ToList(),
                MinAge = auction.MinAge,
                MaxAge = auction.MaxAge,
                StartingPrice = auction.StartingPrice,
            };
        }

        public static Auction Map(this EditAuctionModel model)
        {
            return new Auction
            {
                Id = model.Id,
                InstitutionId = model.InstitutionId,
                Title = model.Title,
                StartingPrice = model.StartingPrice,
                StartDate = DateTime.Now,
                EndDate = model.EndDate,
                Status = "Active",
                Spots = model.Spots,
                Description = model.Description,
                Photos = model.Photos.Select(x => x.Map()).ToList(),
                MinAge = model.HasRequirements ? model.MinAge : null,
                MaxAge = model.HasRequirements ? model.MaxAge : null
            };
        }
        public static ThumbnailAuctionModel Map2(this Auction auction)
        {
            return new ThumbnailAuctionModel
            {
                Id = auction.Id,
                InstitutionId = auction.InstitutionId,
                Title = auction.Title,
                SpotsCount = auction.Spots,
                InstitutionName = auction.Institution.Name,
                PhotoUrl = auction.Photos.Select(x => x.Map()).FirstOrDefault()?.Url,
                EndDate = auction.EndDate,
                Status = auction.Status
            };
        }
    }
}
