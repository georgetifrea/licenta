﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models.Institutions;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Auctions
{
    public class GetAuctionModel
    {
        public Guid Id { get; set; }
        public Guid InstitutionId { get; set; }
        public InstitutionModel Institution { get; set; }
        public string Title { get; set; }
        public double CurrentMaxBid { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public int Spots { get; set; }
        public string Description { get; set; }
        public virtual List<PhotoModel> Photos { get; set; }
        public int NumberOfBidders { get; set; }
        public double StartingPrice { get; set; }
        //requirements
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
    }
}
