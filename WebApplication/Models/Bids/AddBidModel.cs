﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models.Bids
{
    public class AddBidModel
    {
        public Guid AuctionId { get; set; }
        public Guid ProfileId { get; set; }
        public double Value { get; set; }
    }
}
