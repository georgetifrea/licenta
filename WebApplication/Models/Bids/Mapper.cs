﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Auctions;
using WebApplication.Models.Profiles;

namespace WebApplication.Models.Bids
{
    public static class Mapper
    {
        public static BidThumbnail Map(this Bid bid)
        {
            return new BidThumbnail
            {
                Auction = new SimplifiedAuctionModel()
                {
                    Id = bid.AuctionId,
                    InstitutionName = bid.Auction.Institution.Name,
                    PhotoUrl = bid.Auction.Photos.FirstOrDefault().Url,
                    Title = bid.Auction.Title,
                    EndDate = bid.Auction.EndDate
                },
                Profile = bid.Profile?.Map(),
                Value = bid.Value
            };
        }

        public static Bid Map(this AddBidModel bidModel)
        {
            return new Bid
            {
                AuctionId = bidModel.AuctionId,
                ProfileId = bidModel.ProfileId,
                Value = bidModel.Value
            };
        }
    }
}
