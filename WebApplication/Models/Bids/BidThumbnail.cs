﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models.Auctions;
using WebApplication.Models.Profiles;

namespace WebApplication.Models.Bids
{
    public class BidThumbnail
    {
        public double Value { get; set; }
        public GetProfileModel Profile { get; set; }
        public SimplifiedAuctionModel Auction { get; set; }
    }
}
