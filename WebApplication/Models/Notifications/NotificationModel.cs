﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models.Auctions;
using WebApplication.Models.Profiles;

namespace WebApplication.Models.Notifications
{
    public class NotificationModel
    {
        public Guid AuctionId { get; set; }
        public Guid ProfileId { get; set; }
        public string Message { get; set; }
        public DateTime SentAt { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
        public ThumbnailAuctionModel Auction { get; set; }
        public GetProfileModel Profile { get; set; }
    }
}
