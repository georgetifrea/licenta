﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Auctions;
using WebApplication.Models.Profiles;

namespace WebApplication.Models.Notifications
{
    public static class Mapper
    {
        public static NotificationModel Map(this Notification notification)
        {
            return new NotificationModel
            {
                AuctionId = notification.AuctionId,
                Message = notification.Message,
                ProfileId = notification.ProfileId,
                SentAt = notification.SentAt,
                Status = notification.Status,
                Auction = notification.Auction.Map2(),
                Profile = notification.Profile.Map(),
                Result = notification.Result
            };
        }
    }
}
