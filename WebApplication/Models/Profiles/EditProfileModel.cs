﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Profiles
{
    public class EditProfileModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public PhotoModel Photo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime BirthDay { get; set; }
        public string Description { get; set; }
    }
}
