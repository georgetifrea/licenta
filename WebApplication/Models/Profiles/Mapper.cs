﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using WebApplication.Models.Photos;

namespace WebApplication.Models.Profiles
{
    public static class Mapper
    {
        public static Profile Map(this AddProfileModel profileModel)
        {
            return new Profile
            {
                UserId = profileModel.UserId,
                FullName = profileModel.FullName,
                BirthDay = profileModel.BirthDay,
                City = profileModel.City,
                Country = profileModel.Country,
                Photo = profileModel.Photo.Map(),
                Description = profileModel.Description
            };
        }

        public static Profile Map(this EditProfileModel profileModel)
        {
            return new Profile
            {
                Id = profileModel.Id,
                UserId = profileModel.UserId,
                FullName = profileModel.FullName,
                BirthDay = profileModel.BirthDay,
                City = profileModel.City,
                Country = profileModel.Country,
                Photo = profileModel.Photo.Map(),
                Description = profileModel.Description
            };
        }

        public static Profile Map(this GetProfileModel profileModel)
        {
            return new Profile
            {
                Id = profileModel.Id,
                UserId = profileModel.UserId,
                FullName = profileModel.FullName,
                BirthDay = profileModel.BirthDay,
                City = profileModel.City,
                Country = profileModel.Country,
                Photo = profileModel.Photo.Map(),
                Description = profileModel.Description
            };
        }
        public static GetProfileModel Map(this Profile profile)
        {
            return new GetProfileModel
            {
                Id = profile.Id,
                UserId = profile.UserId,
                FullName = profile.FullName,
                BirthDay = profile.BirthDay,
                City = profile.City,
                Country = profile.Country,
                Photo = profile.Photo.Map(),
                Description = profile.Description
            };
        }
    }
}
