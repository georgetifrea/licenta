﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace WebApplication.Models.Addresses
{
    public static class Mapper
    {
        public static CountryModel Map(this Country country)
        {
            return new CountryModel
            {
                Id = country.Id,
                Title = country.Title
            };
        }

        public static CityModel Map(this City city)
        {
            return new CityModel
            {
                Title = city.Title
            };
        }
    }
}
