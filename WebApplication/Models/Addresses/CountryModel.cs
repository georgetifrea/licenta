﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models.Addresses
{
    public class CountryModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
