﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace WebApplication.Models.Photos
{
    public static class Mapper
    {
        public static PhotoModel Map(this Photo photo)
        {
            return new PhotoModel
            {
                Id = photo.Id,
                Url = photo.Url
            };
        }

        public static Photo Map(this PhotoModel photoModel)
        {
            return new Photo
            {
                Id = photoModel.Id,
                Url = photoModel.Url
            };
        }
    }
}
