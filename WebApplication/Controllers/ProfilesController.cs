﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Profiles;

namespace WebApplication.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Produces("application/json")]
    [Route("api/profiles")]
    public class ProfilesController : Controller
    {
        private readonly IProfileRepository _profileRepository;
        public ProfilesController(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddProfileModel model)
        {
            var result = await _profileRepository.InsertAsync(model.Map());
            if (result != null)
            {
                return Ok(result.Map());
            }
            return BadRequest();
        }

        [HttpGet]
        public async Task<List<GetProfileModel>> GetAll([FromQuery] ProfileQuery query)
        {
            var results = (await _profileRepository.Get(query)).Select(x => x.Map()).ToList();
            foreach (var result in results)
            {
                result.InActiveAuction = await _profileRepository.InActiveAuction(result.Id);
            }
            return results;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _profileRepository.GetByIdAsync(id);
            if (result != null)
            {
                return Ok(result.Map());
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<bool> Put([FromBody]EditProfileModel model)
        {
            return await _profileRepository.UpdateAsync(model.Map());
        }
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            //TODO Disable
            await _profileRepository.DeleteAsync(id);
        }

    }
}