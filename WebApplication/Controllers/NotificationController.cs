﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Notifications;

namespace WebApplication.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Produces("application/json")]
    [Route("api/notifications")]
    public class NotificationController : Controller
    {
        private readonly INotificationRepository _notificationRepository;

        public NotificationController(INotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetForUser(Guid id, [FromQuery]int pageIndex, [FromQuery]int results)
        {
            var result = (await _notificationRepository.GetAllForUser(id)).Skip((pageIndex - 1) * results).Take(results);
            if (result != null)
            {
                return Ok(result.Select(x=>x.Map()));
            }
            return BadRequest();

        }
    }
}