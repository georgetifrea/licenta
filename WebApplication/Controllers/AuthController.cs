﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthService;
using AuthService.Helpers;
using Data.Core.Auth;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApplication.Models.Credentials;
using WebApplication.Models.Photos;
using WebApplication.Models.Registrations;

namespace WebApplication.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;
        private readonly IInstitutionRepository _institutionRepository;
        private readonly IPhotoRepository _photoRepository;
        public AuthController(UserManager<User> userManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions, IRoleRepository roleRepository, IUserRepository userRepository, IInstitutionRepository institutionRepository, IPhotoRepository photoRepository)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _institutionRepository = institutionRepository;
            _photoRepository = photoRepository;
            _jwtOptions = jwtOptions.Value;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Post([FromBody]CredentialsModel credentials)
        {
            var userToVerify = await _userManager.FindByNameAsync(credentials.Email);
            if (userToVerify != null)
            {
                if (await _userManager.CheckPasswordAsync(userToVerify, credentials.Password))
                {
                    var identity = _jwtFactory.GenerateClaimsIdentity(userToVerify.UserName, userToVerify.Id);
                    if (identity != null)
                    {
                        return new OkObjectResult(await Tokens.GenerateJwt(
                                                                identity,
                                                                _jwtFactory, new TokenUserModel(userToVerify),
                                                                _jwtOptions)
                                                                );
                    }

                }
            }
            return BadRequest("Invalid username or password.");
        }

        [HttpPost("register/user")]
        public async Task<IActionResult> Post([FromBody]BidderRegistrationModel model)
        {
            var userIdentity = model.Map();
            userIdentity.RoleId = (await _roleRepository.GetRoleByName("Bidder")).Id;

            var result = await _userManager.CreateAsync(userIdentity, model.Password);

            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return new OkObjectResult("Account created");
        }

        [HttpPost("register/institution")]
        public async Task<IActionResult> Post([FromBody]InstitutionRegistrationModel model)
        {
            var userIdentity = model.Map();
            userIdentity.RoleId = (await _roleRepository.GetRoleByName("Institution")).Id;

            var result = await _userManager.CreateAsync(userIdentity, model.Password);

            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return new OkObjectResult("Account created");
        }

        [Authorize(Policy = "ApiUser")]
        [HttpGet("account/user/{id}")]
        public async Task<IActionResult> GetUser(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                return BadRequest();
            }
            return Ok(user.Map2());
        }

        [Authorize(Policy = "ApiUser")]
        [HttpPut("account/user")]
        public async Task<IActionResult> EditUser([FromBody] EditUserModel user)
        {
            var dbUser = await _userRepository.GetByIdAsync(user.Id);
            if (dbUser == null)
            {
                return BadRequest();
            }
            var result = await _userManager.ChangePasswordAsync(dbUser, user.OldPassword, user.NewPassword);
            if (!result.Succeeded)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}