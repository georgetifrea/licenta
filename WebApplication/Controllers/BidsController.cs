﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Bids;

namespace WebApplication.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Produces("application/json")]
    [Route("api/bids")]
    public class BidsController : Controller
    {
        private readonly IBidRepository _bidRepository;
        public BidsController(IBidRepository bidRepository)
        {
            _bidRepository = bidRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddBidModel model)
        {
            var result = await _bidRepository.UpdateOrInsert(model.Map());
            if (result != null)
            {
                return Ok(result.Map());
            }
            return BadRequest();
        }

        [HttpGet]
        public async Task<List<BidThumbnail>> GetAll([FromQuery] BidQuery query)
        {
            var bids =  (await _bidRepository.Get(query)).Select( x =>
            {
                var bid = x.Map();
                bid.Auction.HighBid =  _bidRepository.GetLastWinningBidValue(bid.Auction.Id);
                return bid;
            }).ToList();
            return bids;
        }
    }
}