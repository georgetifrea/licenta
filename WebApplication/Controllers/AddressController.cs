﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Addresses;

namespace WebApplication.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Produces("application/json")]
    [Route("api/address")]
    public class AddressController : Controller
    {
        private readonly ICountryRespository _countryRespository;
        private readonly ICityRepository _cityRepository;
        public AddressController(ICountryRespository countryRespository, ICityRepository cityRepository)
        {
            _countryRespository = countryRespository;
            _cityRepository = cityRepository;
        }

        [HttpGet("countries")]
        public async Task<List<CountryModel>> GetAll()
        {
            return (await _countryRespository.GetAllAsync()).Select(x=>x.Map()).ToList();
        }

        [HttpGet("cities/{id}")]
        public async Task<List<CityModel>> Get(Guid id)
        {
            return (await _cityRepository.GetForCountry(id)).Select(x => x.Map()).ToList();
        }
    }
}