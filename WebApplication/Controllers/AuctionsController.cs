﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Interfaces;
using Data.Core.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Auctions;

namespace WebApplication.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Produces("application/json")]
    [Route("api/auctions")]
    public class AuctionsController : Controller
    {
        private readonly IAuctionRepository _auctionRepository;
        private readonly IBidRepository _bidRepository;

        public AuctionsController(IAuctionRepository auctionRepository, IBidRepository bidRepository)
        {
            _auctionRepository = auctionRepository;
            _bidRepository = bidRepository;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddAuctionModel model)
        {
            var result = await _auctionRepository.InsertAsync(model.Map());
            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var auction = await _auctionRepository.GetByIdAsync(id);
            if (auction != null)
            {
                var model = auction.Map();
                model.NumberOfBidders = await _bidRepository.GetNumberOfBiddersForAuction(auction.Id);
                var maxBidder = await _bidRepository.GetMaxBidderForAuction(auction.Id);
                model.CurrentMaxBid = maxBidder?.Value ?? auction.StartingPrice;
                return Ok(model);
            }
            
            return BadRequest();
        }

        [HttpPut]
        public async Task<bool> Put([FromBody]EditAuctionModel model)
        {
            return await _auctionRepository.UpdateAsync(model.Map());
        }

        [HttpGet]
        public async Task<List<ThumbnailAuctionModel>> GetAll([FromQuery] AuctionQuery query)
        {
            var auctions = (await _auctionRepository.Get(query)).Select(x => x.Map2()).ToList();
            foreach (var auction in auctions)
            {
                auction.BiddersCount = await _bidRepository.GetNumberOfBiddersForAuction(auction.Id);
                auction.HighestBid = (await _bidRepository.GetMaxBidderForAuction(auction.Id))?.Value ?? (await _auctionRepository.GetByIdAsync(auction.Id)).StartingPrice;

            }
            return auctions;
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await _auctionRepository.Cancel(id);
        }
    }
}