﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Core.Domain;

namespace Data.Core.Auth
{
    public class TokenUserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public Guid RoleId { get; set; }
        public Guid? InstitutionId { get; set; }

        public TokenUserModel(User user)
        {
            Id = user.Id;
            UserName = user.UserName;
            RoleName = user.Role.Name;
            RoleId = user.Role.Id;
            InstitutionId = user.Institutions.FirstOrDefault()?.Id;
        }
    }
}
