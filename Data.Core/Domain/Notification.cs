﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Notification : BaseEntity
    {
        public Guid AuctionId { get; set; }
        public virtual Auction Auction { get; set; }
        public Guid ProfileId { get; set; }
        public virtual Profile Profile { get; set; }
        public string Message { get; set; }
        public DateTime SentAt { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
    }
}
