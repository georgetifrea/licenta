﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class City : BaseEntity
    {
        public string Title { get; set; }
        public Guid CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}