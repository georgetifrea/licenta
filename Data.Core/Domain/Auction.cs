﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Auction : BaseEntity
    {
        public Guid InstitutionId { get; set; }
        public virtual Institution Institution { get; set; }
        public string Title { get; set; }
        public double StartingPrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public int Spots { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Bid> Bids { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }

        //requirements
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
    }
}
