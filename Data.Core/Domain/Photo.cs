﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Photo : BaseEntity
    {
        public string Url { get; set; }
    }
}
