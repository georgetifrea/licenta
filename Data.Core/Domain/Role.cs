﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Data.Core.Domain
{
    public class Role : IdentityRole<Guid>
    {
        public virtual ICollection<User> Users { get; set; }
    }
}
