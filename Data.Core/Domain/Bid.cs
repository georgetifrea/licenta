﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Bid : BaseEntity
    {
        public Guid AuctionId { get; set; }
        public virtual Auction Auction { get; set; }
        public Guid ProfileId { get; set; }
        public virtual Profile Profile { get; set; }
        public double Value { get; set; }
    }
}
