﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Country : BaseEntity
    {
        public string Title { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }
}
