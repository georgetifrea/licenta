﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Institution : BaseEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Guid? PhotoId { get; set; }
        public virtual Photo Photo { get; set; }

        //address
        public string Country { get; set; }
        public string City { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }

        public virtual ICollection<Auction> Auctions { get; set; } 
    }
}
