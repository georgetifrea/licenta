﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Domain
{
    public class Profile : BaseEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public string FullName { get; set; }
        public Guid? PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
        public virtual ICollection<Bid> Bids { get; set; }
        public string Description { get; set; }

        //address
        public string Country { get; set; }
        public string City { get; set; }
        //requirements

        public DateTime BirthDay { get; set; }
    }
}
