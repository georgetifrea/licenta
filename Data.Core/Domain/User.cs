﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Data.Core.Domain
{
    public class User : IdentityUser<Guid>
    {
        public DateTime JoinDate { get; set; }
        public Guid RoleId { get; set; }
        public virtual Role Role { get; set; }
        public string Token { get; set; }
        public Guid? PhotoId { get; set; }
        public virtual Photo Photo { get; set; }

        public virtual ICollection<Institution> Institutions { get; set; }
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
