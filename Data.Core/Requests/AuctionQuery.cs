﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Requests
{
    public class AuctionQuery
    {
        public bool Active { get; set; }
        public bool Completed { get; set; }
        public bool Finished { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? JoinedBy { get; set; }
        public double StartDate { get; set; }
        public double EndDate { get; set; }
        public string SearchTerm { get; set; }
        public int PageIndex { get; set; }
        public int Results { get; set; }
        public SortOrder? Order { get; set; }
        public SortCriteria? Criteria { get; set; }
    }
}
