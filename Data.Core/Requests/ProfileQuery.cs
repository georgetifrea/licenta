﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Requests
{
    public class ProfileQuery
    {
        public Guid? CreatedBy { get; set; }
        public int PageIndex { get; set; }
        public int Results { get; set; }
    }
}
