﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core.Requests
{
    public enum SortOrder
    {
        Ascending,
        Descending
    }
}
