﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface IRoleRepository
    {
        Task<Role> GetRoleByName(string name);
    }
}

