﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface ICityRepository : IGenericRepository<City>
    {
        Task<List<City>> GetForCountry(Guid id);
    }
}
