﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Requests;

namespace Data.Core.Interfaces
{
    public interface IProfileRepository :IGenericRepository<Profile>
    {
        Task<List<Profile>> GetProfilesForUser(Guid userId);
        Task<List<Profile>> Get(ProfileQuery query);
        Task<bool> InActiveAuction(Guid id);
    }
}
