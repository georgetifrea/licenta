﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Requests;

namespace Data.Core.Interfaces
{
    public interface IBidRepository: IGenericRepository<Bid>
    {
        Task<Bid> GetMaxBidderForAuction(Guid auctionId);
        Task<int> GetNumberOfBiddersForAuction(Guid auctionId);
        Task<Bid> UpdateOrInsert(Bid bid);
        Task<List<Bid>> Get(BidQuery query);
        double GetLastWinningBidValue(Guid auctionId);
    }
}
