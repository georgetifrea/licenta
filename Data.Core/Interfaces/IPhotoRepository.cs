﻿using System;
using System.Collections.Generic;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface IPhotoRepository : IGenericRepository<Photo>
    {

    }
}
