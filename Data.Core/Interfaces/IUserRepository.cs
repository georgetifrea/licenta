﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<User> UpdateAsync(User user);
        Task<User> GetByIdAsync(Guid id);
    }
}
