﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Core.Domain;
using Data.Core.Requests;

namespace Data.Core.Interfaces
{
    public interface IAuctionRepository : IGenericRepository<Auction>
    {
        Task<List<Auction>> Get(AuctionQuery query);
        Task<bool> Cancel(Guid id);
    }
}
