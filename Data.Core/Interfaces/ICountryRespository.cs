﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface ICountryRespository: IGenericRepository<Country>
    {

    }
}
