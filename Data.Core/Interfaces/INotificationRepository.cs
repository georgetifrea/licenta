﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Core.Domain;

namespace Data.Core.Interfaces
{
    public interface INotificationRepository :IGenericRepository<Notification>
    {
        Task<List<Notification>> GetAllForUser(Guid id);
    }
}
